# Projeto_game

<h1 align="center">Hi 👋, We are Luan. Cristiano. Eduardo</h1>

Desenvolvimento com base no gitflow e divisão de features para cada um.

## Tiled Padronization
<h5>list of object that can be added to the current project</h5>
<p>• Fase: the object where the menu to open a game will pop - the pattern for creation its "Fase" plus "number" example: Fase1 - in the moment the projetc comport 16 fases </p>
<p>• Iterable_item: the object displays an item that will respond to an touch or some interaction, the object can be a chest, a box, etc... the pattern for creation its "Item" plus "number", example: Item1, Item2 (without space between) - to add the type of item see the "Tiled Property" area</p>
<p>• Barrier: the object thats create an barrier that can be removed, the pattern for creation its "Barrier" plus "number", example: Barrier1, Barrier2 (without space between) - to add the type of barrier see the "Tiled Property" area </p>
<p>• Trail: the object displays an image used to demarcate some kind of path  (visual object), the pattern for creation its "Trail" plus "number", example: Trail1</p>

## Tiled Property

<h5>exchange custom properties</h5>
<p>• each object in Tiled has its own properties and custom properties, in this project we need to add custom properties to some types of object</p>
<p>• To add new custom properties: </p>
<p>• Fist, we need to select the object that we want to create the property.</p>
<p>• Then with the object selected, in the properties tab, right click in the custom properties area and select "add property" item.</p>
<p>• A box named "Add Property - Tiled" will pop in the scream, in the first area put the name of the property, in the second you will chose the type of the information that will be put in the property (string, int, float, color, object, file and bool),  ok botton when finish.</p>
<p>• The next step its to put the data in the new property, when you confirm the creation of the property the custom property area will show the new property and an area to put the data that you want to keep</p>

<h5>objets that need to create custom properties</h5>
<p>• Fase: pontuation_limit its a property that is a String and its used to tell which is the parameters to the player to receive a medal - the information that this property give us its in json format and contains this structure: {"gold":90, "silver":70, "copper":50}, the numbers in the json can be changed but the name of medals cannot (to work the data must be write in the exact form);
- pontuation_to_open: its a property that is a String and its used to tell which fase will open based on the type of medal the player gets, the structure of this property its a json and luck like this: {"gold":3, "silver":2, "copper":1.1}, the numbers in the json can be changed but the name of medals cannot (to work the data must be write in the exact form);</p>
<p>•Barrier: have the "type" property that is a String(the types are still in development) </p>
<p>•Iterable_item: have the "type" property that is a String</p>

## Bonfire Modifications

<h5>exchange rootBundle for path_provider functions</h5>
<p>• TiledWorldMap is the principal file for change a map you wish.</p>
<p>• First, we need alter the variable _basePathFlame because it change the "assets/images" prefix.</p>
<p>• Then we need modify the read function of TiledJsonReader, the function stay on _readMap function.</p>
<p>• Then we need modify the read function of TiledJsonReader, the function stay on _readMap function.</p>
<p>• The changes are: add the localPath function and read without rootBundle.</p>

``` 
    syc.Future<String> get _localPath async {
    final directory = await ppo.getApplicationDocumentsDirectory();

    return directory.path;
    }

    syc.Future<String> _localFile(lc) async {
    final path = await _localPath;
    return io.File('$path/$lc').readAsString();
    }...

    ...String data = await _localFile(pathFile);
```

<h5>Change images like Sprites from LocalPathDirectoy</h5>
<p>• We create a class GetDir, this get the image file and tranform for Image</p>
<p>• That class is used on decorations, example</p>

`
    Image undone = await GetDir(lc: '/assets/images/fases/Close $number.png').localFile();
`

<h5>Modify the Flame.image</h5>
<p>• We delete the prefix and work with path_provider too.</p>
<p>• The file we alterate is image.dart from flame1.1.0</p>
<p>• load is the function of load the image, the fetchMemory is responsible for get image.</p>
<p>• We apply the same function of get localPath but return only Uint8List because the flame functions tranform this.</p>

<h3 align="left">Languages and Tools:</h3>
<p align="left"> <a href="https://dart.dev" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/dartlang/dartlang-icon.svg" alt="dart" width="40" height="40"/> </a> <a href="https://flutter.dev" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/flutterio/flutterio-icon.svg" alt="flutter" width="40" height="40"/> </a> </p>
