import 'package:projeto_game/database/database_adaptor.dart';
import 'package:projeto_game/model/fase_pontuation.dart';

const String tableName = 'fasepoints';

class UseCaseFasePontuation {
  final DatabaseAdaptor database = DatabaseAdaptor();

  Future<void> create(FasePontuation model) async {
    final Map<String, dynamic> data = model.toMap();
    await database.saveData(tableName: tableName, data: data);
  }

  Future<List<FasePontuation>> read() async {
    final List<Map<String, dynamic>> data =
        await database.readData(tableName: tableName);
    return data.map((e) => FasePontuation.fromMap(e)).toList();
  }

  Future<void> update(FasePontuation model) async {
    final Map<String, dynamic> data = model.toMap();
    await database.updateData(id: model.id!, data: data, tableName: tableName);
  }

  Future<FasePontuation?> readOnly(int id) async {
    try {
      final List<Map<String, dynamic>> data =
          await database.readData(tableName: tableName, id: id);
      return FasePontuation.fromMap(data[0]);
    } catch (_) {
      return null;
    }
  }
}
