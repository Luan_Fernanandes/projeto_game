class FasePontuation {
  final int? id;
  final String fase;
  final int point;
  final int? open;

  FasePontuation(
      {required this.open, required this.fase, required this.point, this.id});

  factory FasePontuation.fromMap(Map<String, dynamic> json) => FasePontuation(
      fase: json['fase'],
      point: json['point'],
      id: json['id'],
      open: json['open']);

  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "fase": fase,
      "point": point,
      "open": open,
    };
  }
}
