import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';

class GameSpriteSheetCharacter {
  static SpriteAnimation character(Image characterimage) {
    SpriteAnimation anmt = SpriteAnimation.fromFrameData(
      characterimage,
      SpriteAnimationData.sequenced(
        amount: 1,
        stepTime: 1.0,
        textureSize: Vector2(520, 520),
        texturePosition: Vector2(0, 0),
      ),
    );
    return anmt;
  }
}
