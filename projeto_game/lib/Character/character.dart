import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';
import 'sprite_sheet_character.dart';

class Character extends SimplePlayer with ObjectCollision {
  final Image characterimage;
  Character({
    required Vector2 position,
    required Vector2 size,
    required this.characterimage,
  }) : super(
          position: position,
          size: size,
          animation: SimpleDirectionAnimation(
            idleRight: GameSpriteSheetCharacter.character(characterimage),
            runRight: GameSpriteSheetCharacter.character(characterimage),
          ),
        ) {
    aboveComponents = true;
    setupCollision(
      CollisionConfig(
        collisions: [
          CollisionArea.rectangle(
            size: Vector2(30, 35),
            align: Vector2(11, 13),
          ),
        ],
      ),
    );
  }
}
