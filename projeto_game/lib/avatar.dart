import 'dart:typed_data';
import 'package:flutter/material.dart';
import 'package:fluttermoji/fluttermoji.dart';
import 'package:screenshot/screenshot.dart';
import 'download_game_view.dart';
import 'dart:ui' as ui;

class NewPage extends StatefulWidget {
  const NewPage({Key? key}) : super(key: key);

  @override
  State<NewPage> createState() => _NewPageState();
}

class _NewPageState extends State<NewPage> {
  ui.Image? bytes;
  Uint8List? bytessave;
  bool map = false;

  bool btnPlayClick = false;

  static Future<ui.Image> bytesToImage(Uint8List imgBytes) async {
    ui.Codec codec = await ui.instantiateImageCodec(imgBytes);
    ui.FrameInfo frame = await codec.getNextFrame();
    return frame.image;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                child: Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 30),
                      child: Material(
                        color: const Color.fromARGB(0, 133, 28, 28),
                        borderRadius: BorderRadius.circular(100),
                        child: flutteremoji(context),
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 8.0, vertical: 30),
                        child: FluttermojiCustomizer(),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.all(15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: 100,
                      height: 60,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color: Colors.black,
                        ),
                      ),
                      child: const Center(
                        child: Text('Mapa 1'),
                      ),
                    ),
                    Switch(
                      value: map,
                      onChanged: (val) {
                        setState(() {
                          map = val;
                        });
                      },
                    ),
                    Container(
                      width: 100,
                      height: 60,
                      decoration: BoxDecoration(
                        border: Border.all(
                          width: 2,
                          color: Colors.black,
                        ),
                      ),
                      child: const Center(
                        child: Text('Mapa 2'),
                      ),
                    ),
                  ],
                ),
              ),
              !btnPlayClick
                  ? GestureDetector(
                      onTap: () async {
                        setState(() {
                          btnPlayClick = true;
                        });
                        final controller = ScreenshotController();
                        final bytes = await controller.captureFromWidget(
                          Material(
                            color: const Color.fromARGB(0, 133, 28, 28),
                            borderRadius: BorderRadius.circular(100),
                            child: flutteremoji(context),
                          ),
                        );
                        setState(() {
                          bytessave = bytes;
                        });
                        try {
                          await bytesToImage(bytessave!).then((value) {
                            setState(() {
                              this.bytes = value;
                            });
                          });
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DownloadGameView(
                                characterimage: this.bytes!,
                                map: map,
                              ),
                            ),
                          );
                        } catch (e) {
                          setState(() {
                            btnPlayClick = false;
                          });
                        }
                      },
                      child: Container(
                        color: const Color.fromARGB(255, 155, 38, 38),
                        width: 100,
                        height: 60,
                        child: const Center(child: Text('jogar')),
                      ),
                    )
                  : SizedBox(
                      height: 50,
                      width: 250,
                      child: Center(
                        child: Card(
                          color: Colors.lightBlue[400],
                          elevation: 0,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              Container(
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 5),
                                child: const CircularProgressIndicator(
                                  color: Colors.white,
                                ),
                              ),
                              const Expanded(
                                child: Text(
                                  'Aguarde...',
                                  style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                      fontWeight: FontWeight.bold),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}

Widget flutteremoji(context) => Container(
      color: Colors.transparent,
      child: MediaQuery(
        data: const MediaQueryData(),
        child: Container(
          decoration: BoxDecoration(
            color: Colors.transparent,
            borderRadius: BorderRadius.circular(100),
          ),
          child: FluttermojiCircleAvatar(
            radius: 100,
            backgroundColor: Colors.transparent,
          ),
        ),
      ),
    );
