import 'dart:io';

import 'package:archive/archive.dart';
import 'package:dio/dio.dart';

class DownloadFlipBook{
  final String dir;
  final Uri dirtwo;
  DownloadFlipBook({required this.dir, required this.dirtwo});

  late bool _downloading = false;
  final String _zipPath = 'https://luanfern.mxw.com.br/download/flipbook.zip';
  final String _localZipFileName = 'flipbook.zip';

  Future<File> _downloadFile(String url, String fileName) async {
    final req = await Dio().get(
      url,
      options: Options(
        responseType: ResponseType.bytes,
        followRedirects: false,
        receiveTimeout: 0,
      ),
    );
    var file = File('$dir/$fileName');
    return file.writeAsBytes(req.data);
  }

  Future<String> downloadZip() async {
    _downloading = true;

    var zippedFile = await _downloadFile(_zipPath, _localZipFileName);
    await unarchiveAndSave(zippedFile);
    _downloading = false;

    return dirtwo.toString();
  }

  unarchiveAndSave(var zippedFile) async {
    var bytes = zippedFile.readAsBytesSync();
    var archive = ZipDecoder().decodeBytes(bytes);
    for (var file in archive) {
      var fileName = '$dir/${file.name}';
      if (file.isFile) {
        var outFile = File(fileName);
        //print('File:: ' + outFile.path);
        outFile = await outFile.create(recursive: true);
        await outFile.writeAsBytes(file.content);
      }
    }
  }
}