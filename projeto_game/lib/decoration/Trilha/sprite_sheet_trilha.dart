import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';

import '../getDirectory.dart';

class GameSpriteSheetTrilha {
  static Future<Sprite> trilha(String cam) async {
    Image trilhaImage =
        await GetDir(lc: '/assets/images/fases/ponto.png').localFile();
    return Sprite(trilhaImage);
  }
}
