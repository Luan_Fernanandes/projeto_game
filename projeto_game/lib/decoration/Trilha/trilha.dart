import 'package:bonfiremodify/bonfire.dart';
import 'sprite_sheet_trilha.dart';

class Trilha extends GameDecoration {
  bool perto = false;
  late String resfaseid;
  List<dynamic>? rets;

  Trilha(
    String faseid,
    Vector2 position,
    Vector2 size,
    this.rets,
    String cam,
  ) : super.withSprite(
          sprite: GameSpriteSheetTrilha.trilha(cam),
          position: position,
          size: size,
        ) {
    resfaseid = faseid;
    int residconvert = int.parse(resfaseid) - 1;
    resfaseid = residconvert.toString();
    //print(rets);
  }
  /*  @override
  void update(double td) {
    seeComponentType<Character>(
        observed: (player) {
          if (!perto) {
            setfase(resfaseid);
            gameRef.overlays.remove('blockbuttons');
            perto = true;
          }
        },
        notObserved: () {
          if (perto) {
            perto = false;
            gameRef.overlays.remove('gameMessage');
            gameRef.overlays.add('blockbuttons');
          }
        },
        radiusVision: 4);
    super.update(td);
  } */
}
