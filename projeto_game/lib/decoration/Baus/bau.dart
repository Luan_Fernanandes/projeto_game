import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import '../../Character/character.dart';
import '../../audio/sounds_manager.dart';
import '../medalhas/medalha.dart';
import 'sprite_sheet_bau.dart';

class Chess extends GameDecoration with TapGesture {
  bool playerIsCloses = false;
  Sprite? chess, chessOpen;
  final String cam;

  Chess({
    required Vector2 position,
    required Vector2 size,
    required this.cam,
  }) : super.withSprite(
          sprite: DecorationSpriteSheet.chess(cam),
          position: position,
          size: Vector2(50, 50),
        );
  @override
  void update(double dt) {
    seeComponentType<Character>(
      observed: (player) {
        if (!playerIsCloses) {
          playerIsCloses = true;
          _showDialog();
        }
      },
      notObserved: () {
        playerIsCloses = false;
      },
      radiusVision: 1,
    );
    super.update(dt);
  }

  @override
  void render(Canvas canvas) {
    if (playerIsCloses) {
      sprite = chessOpen;
    } else {
      sprite = chess;
    }
    super.render(canvas);
  }

  @override
  Future<void> onLoad() async {
    chess = await DecorationSpriteSheet.chess(cam);
    chessOpen = await DecorationSpriteSheet.chessOpen(cam);
    return super.onLoad();
  }

  void _showDialog() {
    //print('baú vazio ');
  }

  @override
  void onTap() {
    if (playerIsCloses) {
      //_showDialog();
      gameRef.add(Medal(position: position, size: position, cam: cam));
      SoundsManager.buttonClick();
    }
  }

  @override
  void onTapCancel() {
    // TODO: implement onTapCancel
  }

  @override
  void onTapDown(int pointer, Vector2 position) {
    // TODO: implement onTapDown
  }

  @override
  void onTapUp(int pointer, Vector2 position) {
    // TODO: implement onTapUp
  }
}
