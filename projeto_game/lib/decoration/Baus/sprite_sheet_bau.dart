import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';

import '../getDirectory.dart';

class DecorationSpriteSheet {
  static Future<Sprite> chess(String cam) async {
    Image chessImage =
        await GetDir(lc: '/assets/images/bau_fechado.png').localFile();
    return Sprite(
      chessImage,
      srcPosition: Vector2(0, 0),
      srcSize: Vector2(106, 106),
    );
  }

  static Future<Sprite> chessOpen(String cam) async {
    Image chessOpenImage =
        await GetDir(lc: '/assets/images/bau_aberto.png').localFile();
    return Sprite(
      chessOpenImage,
      srcPosition: Vector2(0, 0),
      srcSize: Vector2(106, 106),
    );
  }
}
