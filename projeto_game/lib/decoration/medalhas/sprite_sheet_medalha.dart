import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';

import '../getDirectory.dart';

class SpriteSheetMedal {
  static Future<Sprite> medal(String cam) async {
    Image medalImage =
        await GetDir(lc: '/assets/images/medalha pronta.png').localFile();
    return Sprite(
      medalImage,
      srcPosition: Vector2(0, 0),
      srcSize: Vector2(288, 288),
    );
  }
}
