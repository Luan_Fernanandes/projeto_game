import 'package:bonfiremodify/bonfire.dart';

import '../../Character/character.dart';
import 'sprite_sheet_medalha.dart';

class Medal extends GameDecoration with Sensor {
  Medal({
    required Vector2 position,
    required Vector2 size,
    required String cam,
  }) : super.withSprite(
          sprite: SpriteSheetMedal.medal(cam),
          position: position,
          size: Vector2(22, 22),
        );

  @override
  void onContact(GameComponent component) {
    if (component is Character) {
      (component).addLife(0);
      removeFromParent();
    }
  }

  @override
  void onMount() {
    final initialPosition = position;
    const deslocamento = 0;
    gameRef.getValueGenerator(
      const Duration(milliseconds: 500),
      onChange: (value) {
        // double newX = Curves.ease.transform(value);
        position = initialPosition.translate(deslocamento * value, 2);
      },
    ).start();
    super.onMount();
  }
}
