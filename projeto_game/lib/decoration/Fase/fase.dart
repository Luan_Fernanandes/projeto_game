import 'dart:convert';
import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart' as md;
import 'package:flutter_modular/flutter_modular.dart';
import 'package:projeto_game/decoration/Fase/sprite_sheet_fase.dart';
import 'package:projeto_game/store/mobx_store.dart';

import '../../Character/character.dart';
import '../../interface/barrier_message.dart';
import '../../store/mobx_store.dart';

class Fase extends GameDecoration {
  bool perto = false;
  late String resfaseid;
  List<dynamic>? rets;
  String faseid;
  String cam;
  late int? fasePoint;
  final AppStore store = Modular.get();
  Function fncteste;

  Sprite? undone, silver, gold, copper;

  Fase(
    this.faseid,
    Vector2 position,
    Vector2 size,
    this.rets,
    this.cam,
    bool? visible,
    this.fncteste,
  ) : super.withSprite(
          sprite: GameSpriteSheetFase.faseUndone(faseid),
          position: position,
          size: size,
        ) {
          print('visible: $visible $faseid');
          visible = false;
          print('visible: $visible $faseid');
    resfaseid = faseid;
    int residconvert = int.parse(resfaseid) - 1;
    resfaseid = residconvert.toString();
  }

  @override
  Future<void> onLoad() async {
    fasePoint = await store.getPointsFromDb(faseid);
    print(fasePoint);
    undone = await GameSpriteSheetFase.faseUndone(faseid);
    gold = await GameSpriteSheetFase.unionDone(
        await GameSpriteSheetFase.goldmedal(cam),
        await GameSpriteSheetFase.faseDone(faseid));
    silver = await GameSpriteSheetFase.unionDone(
        await GameSpriteSheetFase.silvermelda(cam),
        await GameSpriteSheetFase.faseDone(faseid));
    copper = await GameSpriteSheetFase.unionDone(
        await GameSpriteSheetFase.coppermedal(cam),
        await GameSpriteSheetFase.faseDone(faseid));
    return super.onLoad();
  }

  @override
  void render(Canvas canvas) {
    if (store.openFase.contains(faseid)) {
      if (fasePoint != null &&
          fasePoint! >= jsonDecode(rets![2]['pontuation_limit'])['gold']) {
        sprite = gold;
      } else if (fasePoint != null &&
          fasePoint! >= jsonDecode(rets![2]['pontuation_limit'])['silver'] &&
          fasePoint! <= jsonDecode(rets![2]['pontuation_limit'])['gold']) {
        sprite = silver;
      } else if (fasePoint != null &&
          fasePoint! >= jsonDecode(rets![2]['pontuation_limit'])['copper'] &&
          fasePoint! <= jsonDecode(rets![2]['pontuation_limit'])['silver']) {
        sprite = copper;
      } else {
        sprite = undone;
      }
    } else {
      sprite = undone;
    }
    super.render(canvas);
  }

  @override
  void update(double td) {
    seeComponentType<Character>(
        observed: (player) async {
          if (!perto) {
            fncteste;
            print('fez');
            perto = true;
            store.setMedalPoints(rets![2]['pontuation_limit']);
            store.setFaseJump(rets![2]['pontuation_to_open']);
            store.setFaseNumber(faseid);
            print(faseid);
            print(store.faseNumber);
            store.setUrl(
              rets![2]['game_url'] ??
                  'http://www.fmodia.com.br/semana-maluca/files/2018/04/sem-foto.png',
            );
            if (faseid != "1") {
              FollowerWidget.remove('identify');
              final isOpen = await store.getOpenFromDb();
              if (isOpen == null || isOpen == 0) {
                FollowerWidget.show(
                    context: context,
                    identify: 'identify',
                    target: this,
                    child: barrierMessage());
              } else {
                gameRef.overlays.add('showGameWidget');
              }
            } else {
              gameRef.overlays.add('showGameWidget');
            }
          }
        },
        notObserved: () async {
          if (perto) {
            fncteste;
            perto = false;
            gameRef.overlays.remove('showGameWidget');
            fasePoint = await store.getPointsFromDb(faseid);
          }
        },
        radiusVision: 4);
    super.update(td);
  }
}
