import 'package:bonfiremodify/bonfire.dart';
import '../getDirectory.dart';
import 'dart:ui' as ui;

class GameSpriteSheetFase {
  static Future<ui.Image> faseDone(String number) async {
    var base = await GetDir(lc: '/assets/images/fases/Open $number.png').localFile();
    return base;
  }

  static Future<Sprite> faseUndone(String number) async {
    var base = await GetDir(lc: '/assets/images/fases/Close $number.png').localFile();
    return Sprite(base);
  }

  static Future<ui.Image> goldmedal(String cam) async {
    return await GetDir(lc: '/assets/images/medalha-pronta-map.png')
        .localFile();
    //return await Flame.images.load('${cam}assets/images/medalha-pronta-map.png');
  }

  static Future<ui.Image> silvermelda(String cam) async {
    return await GetDir(lc: '/assets/images/medalha-cinza-map.png').localFile();
    //return await Flame.images.load('${cam}assets/images/medalha-cinza-map.png');
  }

  static Future<ui.Image> coppermedal(String cam) async {
    return await GetDir(lc: '/assets/images/medalha-bronze-map.png')
        .localFile();
    //return await Flame.images.load('${cam}assets/images/medalha-bronze-map.png');
  }

  static Future<Sprite> unionDone(ui.Image medal, ui.Image base) async {
    ui.Image finalDone = await base.overlap(medal);
    return Sprite(finalDone);
  }
}
