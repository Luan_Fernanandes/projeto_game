import 'dart:ui';
import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter_modular/flutter_modular.dart';
import '../../Character/character.dart';
import '../../decoration/barreira/sprite_sheet_barrier.dart';
import '../../interface/barrier_message.dart';
import '../../store/mobx_store.dart';

class Barrier extends GameDecoration with ObjectCollision {
  final String cam;
  Barrier({
    required Vector2 position,
    required Vector2 size,
    required this.cam,
  }) : super.withSprite(
            sprite: BarrierSprite.montainDefaut,
            position: position,
            size: size) {
    setupCollision(CollisionConfig(
      enable: true,
      collisions: [
        CollisionArea.rectangle(size: Vector2(32, 35), align: Vector2(0, 0)),
      ],
    ));
  }
  bool perto = false;
  Sprite? montanha1, montanha2, montanha3, espacoVazio;
  final AppStore store = Modular.get();
  bool complete = false;

  @override
  Future<void> onLoad() async {
    montanha1 = await BarrierSprite.montainDefaut;
    montanha3 = await BarrierSprite.montain2;
    montanha2 = await BarrierSprite.montain3;
    espacoVazio = await BarrierSprite.montain4;
    return super.onLoad();
  }

  @override
  void render(Canvas canvas) async {
    if (perto) {
      sprite = espacoVazio;
    } else if (!perto && store.rPoints < 70) {
      sprite = montanha1;
    } else if (complete && store.rPoints > 70) {
      sprite = espacoVazio;
    }
    super.render(canvas);
  }

  @override
  bool onCollision(GameComponent component, bool active) {
    if (store.rPoints >= 70) {
      gameRef.add(AnimatedObjectOnce(
          position: position,
          size: Vector2(32, 35),
          animation: SpriteAnimation.spriteList(
              [montanha1!, montanha2!, montanha3!, espacoVazio!],
              stepTime: 0.3)));

      gameRef.visibleComponentsByType<Barrier>().first.collisionConfig!.enable =
          false;
      complete = true;
    }

    return super.onCollision(component, active);
  }

  @override
  void update(double dt) {
    seeComponentType<Character>(
        observed: (player) {
          if (!perto && store.rPoints < 70) {
            perto = true;

            FollowerWidget.show(
                context: context,
                identify: 'message',
                target: this,
                child: barrierMessage());
          }
        },
        notObserved: () {
          if (perto) {
            perto = false;
            FollowerWidget.remove('message');
          }
        },
        radiusVision: 10);
    super.update(dt);
  }
}
