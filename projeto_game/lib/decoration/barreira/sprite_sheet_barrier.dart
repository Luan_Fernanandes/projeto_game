import 'dart:ui';

import 'package:bonfiremodify/bonfire.dart';
import '../getDirectory.dart';

class BarrierSprite {
  static Future<Sprite> get montainDefaut async {
    Image trunkImage =
        await GetDir(lc: '/assets/images/montain_rock.png').localFile();
    return Sprite(
      trunkImage,
      srcPosition: Vector2(64, 80),
      srcSize: Vector2(32, 32),
    );
  }

  static Future<Sprite> get montain2 async {
    Image trunkImage =
        await GetDir(lc: '/assets/images/montain_rock.png').localFile();
    return Sprite(
      trunkImage,
      srcPosition: Vector2(64, 112),
      srcSize: Vector2(16, 16),
    );
  }

  static Future<Sprite> get montain3 async {
    Image trunkImage =
        await GetDir(lc: '/assets/images/montain_rock.png').localFile();
    return Sprite(
      trunkImage,
      srcPosition: Vector2(80, 112),
      srcSize: Vector2(16, 16),
    );
  }

  static Future<Sprite> get montain4 async {
    Image trunkImage =
        await GetDir(lc: '/assets/images/montain_rock.png').localFile();
    return Sprite(
      trunkImage,
      srcPosition: Vector2(96, 80),
      srcSize: Vector2(16, 16),
    );
  }
}
