import 'dart:typed_data';

import 'package:path_provider/path_provider.dart' as ppo; 
import 'dart:io' as io;
import 'dart:async' as syc;
import 'dart:ui' as ui;

class GetDir {
  final String lc;
  GetDir({
    required this.lc,
  });

  syc.Future<String> get _localPath async {
    final directory = await ppo.getApplicationDocumentsDirectory();

    return directory.path;
  }

  syc.Future<ui.Image> localFile() async {
    final path = await _localPath;
    ui.Codec codec = await ui.instantiateImageCodec(io.File('$path/$lc').readAsBytesSync());
    ui.FrameInfo frame = await codec.getNextFrame();
    return frame.image;
  }

   syc.Future<Uint8List> localFileShowWidget() async {
    final path = await _localPath;
    return io.File('$path/$lc').readAsBytesSync();
  }
}
