// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mobx_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AppStore on _AppStoreBase, Store {
  Computed<String>? _$faseComputed;

  @override
  String get fase => (_$faseComputed ??=
          Computed<String>(() => super.fase, name: '_AppStoreBase.fase'))
      .value;
  Computed<String>? _$urlComputed;

  @override
  String get url => (_$urlComputed ??=
          Computed<String>(() => super.url, name: '_AppStoreBase.url'))
      .value;
  Computed<Map<String, dynamic>>? _$mPointsComputed;

  @override
  Map<String, dynamic> get mPoints =>
      (_$mPointsComputed ??= Computed<Map<String, dynamic>>(() => super.mPoints,
              name: '_AppStoreBase.mPoints'))
          .value;
  Computed<Map<String, dynamic>?>? _$fJumpComputed;

  @override
  Map<String, dynamic>? get fJump =>
      (_$fJumpComputed ??= Computed<Map<String, dynamic>?>(() => super.fJump,
              name: '_AppStoreBase.fJump'))
          .value;
  Computed<int>? _$rPointsComputed;

  @override
  int get rPoints => (_$rPointsComputed ??=
          Computed<int>(() => super.rPoints, name: '_AppStoreBase.rPoints'))
      .value;

  final _$fasesAtom = Atom(name: '_AppStoreBase.fases');

  @override
  List<FasePontuation> get fases {
    _$fasesAtom.reportRead();
    return super.fases;
  }

  @override
  set fases(List<FasePontuation> value) {
    _$fasesAtom.reportWrite(value, super.fases, () {
      super.fases = value;
    });
  }

  final _$openFaseAtom = Atom(name: '_AppStoreBase.openFase');

  @override
  List<String> get openFase {
    _$openFaseAtom.reportRead();
    return super.openFase;
  }

  @override
  set openFase(List<String> value) {
    _$openFaseAtom.reportWrite(value, super.openFase, () {
      super.openFase = value;
    });
  }

  final _$jumpFaseAtom = Atom(name: '_AppStoreBase.jumpFase');

  @override
  int get jumpFase {
    _$jumpFaseAtom.reportRead();
    return super.jumpFase;
  }

  @override
  set jumpFase(int value) {
    _$jumpFaseAtom.reportWrite(value, super.jumpFase, () {
      super.jumpFase = value;
    });
  }

  final _$imageUrlAtom = Atom(name: '_AppStoreBase.imageUrl');

  @override
  String get imageUrl {
    _$imageUrlAtom.reportRead();
    return super.imageUrl;
  }

  @override
  set imageUrl(String value) {
    _$imageUrlAtom.reportWrite(value, super.imageUrl, () {
      super.imageUrl = value;
    });
  }

  final _$medalPointsAtom = Atom(name: '_AppStoreBase.medalPoints');

  @override
  Map<String, dynamic> get medalPoints {
    _$medalPointsAtom.reportRead();
    return super.medalPoints;
  }

  @override
  set medalPoints(Map<String, dynamic> value) {
    _$medalPointsAtom.reportWrite(value, super.medalPoints, () {
      super.medalPoints = value;
    });
  }

  final _$faseJumpAtom = Atom(name: '_AppStoreBase.faseJump');

  @override
  Map<String, dynamic>? get faseJump {
    _$faseJumpAtom.reportRead();
    return super.faseJump;
  }

  @override
  set faseJump(Map<String, dynamic>? value) {
    _$faseJumpAtom.reportWrite(value, super.faseJump, () {
      super.faseJump = value;
    });
  }

  final _$faseNumberAtom = Atom(name: '_AppStoreBase.faseNumber');

  @override
  String get faseNumber {
    _$faseNumberAtom.reportRead();
    return super.faseNumber;
  }

  @override
  set faseNumber(String value) {
    _$faseNumberAtom.reportWrite(value, super.faseNumber, () {
      super.faseNumber = value;
    });
  }

  final _$randomPointsAtom = Atom(name: '_AppStoreBase.randomPoints');

  @override
  int get randomPoints {
    _$randomPointsAtom.reportRead();
    return super.randomPoints;
  }

  @override
  set randomPoints(int value) {
    _$randomPointsAtom.reportWrite(value, super.randomPoints, () {
      super.randomPoints = value;
    });
  }

  final _$setOpenFaseAsyncAction = AsyncAction('_AppStoreBase.setOpenFase');

  @override
  Future<void> setOpenFase() {
    return _$setOpenFaseAsyncAction.run(() => super.setOpenFase());
  }

  final _$getPointsFromDbAsyncAction =
      AsyncAction('_AppStoreBase.getPointsFromDb');

  @override
  Future<int?> getPointsFromDb(String fase) {
    return _$getPointsFromDbAsyncAction.run(() => super.getPointsFromDb(fase));
  }

  final _$getOpenFromDbAsyncAction = AsyncAction('_AppStoreBase.getOpenFromDb');

  @override
  Future<int?> getOpenFromDb() {
    return _$getOpenFromDbAsyncAction.run(() => super.getOpenFromDb());
  }

  final _$generatePointsAsyncAction =
      AsyncAction('_AppStoreBase.generatePoints');

  @override
  Future<void> generatePoints(int number) {
    return _$generatePointsAsyncAction.run(() => super.generatePoints(number));
  }

  final _$_AppStoreBaseActionController =
      ActionController(name: '_AppStoreBase');

  @override
  void setUrl(String newUrl) {
    final _$actionInfo = _$_AppStoreBaseActionController.startAction(
        name: '_AppStoreBase.setUrl');
    try {
      return super.setUrl(newUrl);
    } finally {
      _$_AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setFaseNumber(String newFase) {
    final _$actionInfo = _$_AppStoreBaseActionController.startAction(
        name: '_AppStoreBase.setFaseNumber');
    try {
      return super.setFaseNumber(newFase);
    } finally {
      _$_AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setMedalPoints(String newJson) {
    final _$actionInfo = _$_AppStoreBaseActionController.startAction(
        name: '_AppStoreBase.setMedalPoints');
    try {
      return super.setMedalPoints(newJson);
    } finally {
      _$_AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setFaseJump(String? json) {
    final _$actionInfo = _$_AppStoreBaseActionController.startAction(
        name: '_AppStoreBase.setFaseJump');
    try {
      return super.setFaseJump(json);
    } finally {
      _$_AppStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
fases: ${fases},
openFase: ${openFase},
jumpFase: ${jumpFase},
imageUrl: ${imageUrl},
medalPoints: ${medalPoints},
faseJump: ${faseJump},
faseNumber: ${faseNumber},
randomPoints: ${randomPoints},
fase: ${fase},
url: ${url},
mPoints: ${mPoints},
fJump: ${fJump},
rPoints: ${rPoints}
    ''';
  }
}
