import 'dart:convert';

import 'package:mobx/mobx.dart';
import 'package:projeto_game/model/fase_pontuation.dart';
import 'package:projeto_game/model/use_case_fase_pontuation.dart';

part 'mobx_store.g.dart';

class AppStore = _AppStoreBase with _$AppStore;

abstract class _AppStoreBase with Store {
  final UseCaseFasePontuation _fasePontuation = UseCaseFasePontuation();

  @observable
  List<FasePontuation> fases = [];

  @observable
  List<String> openFase = [];

  @observable
  int jumpFase = 0;

  @observable
  String imageUrl = "";

  @observable
  Map<String, dynamic> medalPoints = {};

  @observable
  Map<String, dynamic>? faseJump = {};

  @observable
  String faseNumber = "";

  @observable
  int randomPoints = 0;

  @computed
  String get fase => faseNumber;

  @computed
  String get url => imageUrl;

  @computed
  Map<String, dynamic> get mPoints => medalPoints;

  @computed
  Map<String, dynamic>? get fJump => faseJump;

  @computed
  int get rPoints => randomPoints;

  @action
  void setUrl(String newUrl) {
    imageUrl = newUrl;
  }

  @action
  void setFaseNumber(String newFase) {
    faseNumber = newFase;
  }

  @action
  void setMedalPoints(String newJson) {
    medalPoints = jsonDecode(newJson);
  }

  @action
  void setFaseJump(String? json) {
    if (json == null) {
      faseJump = null;
    } else {
      faseJump = jsonDecode(json);
    }
  }

  @action
  Future<void> setOpenFase() async {
    final response = await _fasePontuation.read();
    fases = response;
    final fasesAbertas = (response.where((e) => e.open == 1).toList());
    List<String> list = [];
    for (var fase in fasesAbertas) {
      list.add(fase.fase);
    }
    openFase = list;
  }

  @action
  Future<int?> getPointsFromDb(String fase) async {
    final FasePontuation? result =
        await _fasePontuation.readOnly(int.parse(fase));
    if (result == null) {
      return null;
    } else {
      return result.point;
    }
  }

  @action
  Future<int?> getOpenFromDb() async {
    print(faseNumber);
    final FasePontuation? result =
        await _fasePontuation.readOnly(int.parse(faseNumber));
    print("result $result");
    if (result == null) {
      return null;
    } else {
      return result.open;
    }
  }

  @action
  Future<void> generatePoints(int number) async {
    randomPoints = number;
    print(fase);
    print(faseNumber);
    _fasePontuation.create(FasePontuation(
        fase: faseNumber, point: number, id: int.parse(faseNumber), open: 1));
    fases = await _fasePontuation.read();
    List<int?> faseAlreadExists = fases.map((e) => e.id).toList();
    print(faseAlreadExists);
    if (number >= medalPoints['gold'] &&
        !faseAlreadExists
            .contains(fJump?['gold'] ?? int.parse(faseNumber) + 1)) {
      _fasePontuation.create(FasePontuation(
          open: 1,
          fase: fJump?['gold'].toString() ??
              (int.parse(faseNumber) + 1).toString(),
          point: 0,
          id: fJump?['gold'] ?? int.parse(faseNumber) + 1));
      jumpFase = fJump?['gold'] ?? int.parse(faseNumber) + 1;
    }
    if (number >= medalPoints['silver'] &&
        number < medalPoints['gold'] &&
        !faseAlreadExists
            .contains(fJump?['silver'] ?? int.parse(faseNumber) + 1)) {
      print('entrou no silver');
      _fasePontuation.create(FasePontuation(
          open: 1,
          fase: fJump?['silver']?.toString() ??
              (int.parse(faseNumber) + 1).toString(),
          point: 0,
          id: fJump?['silver'] ?? int.parse(faseNumber) + 1));
      jumpFase = fJump?['silver'] ?? int.parse(faseNumber) + 1;
    }
    if (number >= medalPoints['copper'] &&
        number < medalPoints['silver'] &&
        !faseAlreadExists
            .contains(fJump?['copper'] ?? int.parse(faseNumber) + 1)) {
      print('entrou no copper');
      _fasePontuation.create(FasePontuation(
          open: 1,
          fase: fJump?['copper'].toString() ??
              (int.parse(faseNumber) + 1).toString(),
          point: 0,
          id: fJump?['copper'] ?? int.parse(faseNumber) + 1));
      jumpFase = fJump?['copper'] ?? int.parse(faseNumber) + 1;
    }
  }
}
