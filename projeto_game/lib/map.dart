import 'dart:typed_data';

import 'package:bonfiremodify/bonfire.dart';
import 'package:bonfiremodify/tiled/model/tiled_object_properties.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:projeto_game/decoration/barreira/barrier.dart';
import 'package:projeto_game/interface/show_game_widget.dart';
import 'package:projeto_game/store/mobx_store.dart';

import 'Character/character.dart';
import 'avatar.dart';
import 'decoration/Baus/bau.dart';
import 'decoration/Fase/fase.dart';
import 'decoration/Trilha/trilha.dart';
import 'decoration/getDirectory.dart';
import 'interface/buttons_interface.dart';
import 'interface/congrats_message.dart';
import 'interface/fases_menu.dart';
import 'interface/loading_interface.dart';
import 'store/mobx_store.dart';
import 'dart:ui' as ui;

import 'audio/sounds_manager.dart';

class Map extends StatefulWidget {
  final ui.Image characterimage;
  final String cam;
  final String folderName;
  const Map({
    Key? key,
    required this.characterimage,
    required this.cam,
    required this.folderName,
  }) : super(key: key);

  @override
  State<Map> createState() => _MapState();
}

class _MapState extends ModularState<Map, AppStore> {
  @override
  void initState() {
    loadMedals('medalha-vazia.png');
    loadMedals('medalha bronze.png');
    loadMedals('medalha cinza.png');
    loadMedals('medalha pronta.png');
    rodarMusica();
    super.initState();
  }

  rodarMusica() async {
    await SoundsManager.initialize();
    //await SoundsManager.playChopinRevelation();
  }

  List<Uint8List> medalsImageGet = [];

  loadMedals(String medalaName) async {
    await GetDir(lc: '/assets/images/$medalaName').localFileShowWidget().then(
          (value) => medalsImageGet.add(value),
        );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BonfireTiledWidget(
            progress: transition(widget.cam),
            onReady: (game) {
              print('Pronto');
            },
            overlayBuilderMap: {
              'FasesMenu': (context, game) => FasesMenu(
                    game: game,
                  ),
              'showGameWidget': (context, game) => ShowGameWidget(
                  urlImage: store.imageUrl, game: game, fase: store.fase),
              'backbutton': (context, game) => backButton(game, () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const NewPage(),
                      ),
                    );
                  }),
              'longButton1': (context, game) => longButtonWithoutPng(
                  game, () {}, 'Atividades', const Alignment(-0.5, -0.84)),
              'longButton2': (context, game) => longButtonWithoutPng(
                  game, () {}, 'Desenpenho', const Alignment(0, -0.84)),
              'longButton3': (context, game) => longButtonWithoutPng(
                  game, () {}, 'Biblioteca', const Alignment(0.5, -0.84)),
              'circularButton': (context, game) =>
                  circleAvatarButtonWithGear(game, () {}),
              'loading': (context, game) => Loading(game: game),
              'CongratsMessage': (context, game) => CongratsMessage(
                    game: game,
                    faseNumber: '7',
                    score: 95,
                    status: true,
                    medalsImage: medalsImageGet,
                  ),
            },
            initialActiveOverlays: const [
              'FasesMenu',
              'backbutton',
              'longButton1',
              'longButton2',
              'longButton3',
              'circularButton',
            ],
            joystick: JoystickMoveToPosition(),
            map: TiledWorldMap(
              //${widget.cam}
              'assets/images/map/projeto_mapa_escribo.json',
              objectsBuilder: {
                'barreira1': (properties) => Barrier(
                      position: properties.position,
                      size: Vector2(32, 35),
                      cam: widget.cam,
                    ),
                'trunk': (TiledObjectProperties properties) => Chess(
                      position: properties.position,
                      size: Vector2(40, 90),
                      cam: widget.cam,
                    ),
                for (var i = 0; i < 16; i++)
                  'Fase${i + 1}': (TiledObjectProperties properties) {
                    return Fase(
                      "${i + 1}",
                      properties.position,
                      properties.size * 1.6,
                      //Vector2(70, 70),
                      [
                        properties.type,
                        properties.id,
                        properties.others,
                      ],
                      widget.cam,
                      properties.visible,
                      properties.isVisibleSwitch,
                    );
                  },
                for (var i = 0; i < 53; i++)
                  '${i + 1}': (TiledObjectProperties properties) => Trilha(
                        "${i + 1}",
                        Vector2(
                            properties.position.x - 20, properties.position.y - 20),
                        Vector2(40, 40),
                        [
                          properties.type,
                          properties.id,
                          properties.others,
                        ],
                        widget.cam,
                      ),
              },
              forceTileSize: const Size(16, 16),
            ),
            player: Character(
              position: Vector2(2, 17 * 16),
              size: Vector2(50, 50),
              characterimage: widget.characterimage,
            ),
            cameraConfig: CameraConfig(
              zoom: 1.25,
              moveOnlyMapArea: true,
              sizeMovementWindow: Vector2(50, 50),
              smoothCameraEnabled: true,
            ),
            //lightingColorGame: const Color.fromRGBO(0, 0, 0, 0.3),
            //showCollisionArea: true,
      ),
    );
  }
}

Widget transition(cam) {
  return Container(
    width: double.infinity,
    height: double.infinity,
    decoration: const BoxDecoration(
        /* image: DecorationImage(
            image: FileImage(img),
            fit: BoxFit.cover), */
        ),
    child: Center(
      child: Container(
        height: 40,
        color: Colors.white.withOpacity(0.7),
        child: const LinearProgressIndicator(),
      ),
    ),
  );
}
