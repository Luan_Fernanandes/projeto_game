import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'package:projeto_game/store/mobx_store.dart';

class ShowGameWidget extends StatefulWidget {
  final String fase;
  final BonfireGame game;
  final String? urlImage;
  const ShowGameWidget(
      {Key? key,
      required this.urlImage,
      required this.game,
      required this.fase})
      : super(key: key);

  @override
  State<ShowGameWidget> createState() => _ShowGameWidgetState();
}

class _ShowGameWidgetState extends State<ShowGameWidget> {
  @override
  void initState() {
    widget.game.pauseEngine();
    print('paused');
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final AppStore store = Modular.get();
    /* print('no showgame');
    print(store.mPoints); */
    return Align(
      alignment: const Alignment(0.2, 0),
      child: Stack(children: [
        Container(
          width: size.width * 0.80,
          height: size.height * 0.7,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10), color: Colors.blue[300]),
        ),
        Container(
          width: size.width * 0.80,
          height: size.height * 0.7,
          padding: const EdgeInsets.all(10),
          child: Column(
            children: [
              Expanded(
                child: Container(
                  width: size.width * 0.80,
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          blurRadius: 6,
                          color: Colors.grey[800]!.withOpacity(0.6),
                          offset: const Offset(2, 2),
                          spreadRadius: -1,
                        ),
                      ],
                      borderRadius: BorderRadius.circular(10),
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  child: Image.network(
                    widget.urlImage ?? '',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
                  height: size.height * 0.16,
                  width: size.width * 0.76,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: const Color.fromARGB(255, 255, 255, 255)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Card(
                          elevation: 0,
                          child: Text(
                            'Fase Numero ${widget.fase}',
                            style: const TextStyle(
                                color: Colors.blue,
                                fontSize: 32,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(right: 16.0),
                        child: GestureDetector(
                          onTap: () async {
                            await store.generatePoints(91);

                            widget.game.overlays.remove('showGameWidget');
                            widget.game.overlays.add('CongratsMessage');
                          },
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(10),
                                color: Colors.blue[600],
                                boxShadow: [
                                  BoxShadow(
                                    blurRadius: 6,
                                    color: Colors.grey[800]!.withOpacity(0.6),
                                    offset: const Offset(2, 2),
                                    spreadRadius: -1,
                                  ),
                                ]),
                            height: size.height * 0.12,
                            width: size.width * 0.2,
                            child: Card(
                              color: Colors.blue[600],
                              elevation: 0,
                              child: const Center(
                                child: Text(
                                  'Jogar',
                                  style: TextStyle(
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
        Positioned(
            right: 3,
            top: 3,
            child: Material(
              color: Colors.blue[600],
              borderRadius: BorderRadius.circular(100),
              child: IconButton(
                  onPressed: () {
                    widget.game.overlays.remove('showGameWidget');
                    widget.game.resumeEngine();
                    widget.game.joystick?.moveTo(
                      Vector2(
                        widget.game.player!.x,
                        widget.game.player!.y,
                      ),
                    );
                    print('resume');
                  },
                  icon: const Icon(
                    Icons.close,
                    color: Colors.white,
                  )),
            )),
      ]),
    );
  }
}
