import 'dart:typed_data';
import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'dart:ui' as ui;

import 'package:projeto_game/store/mobx_store.dart';

class CongratsMessage extends StatefulWidget {
  final BonfireGame game;
  final bool status;
  final String faseNumber;
  final int score;
  final List<Uint8List> medalsImage;
  const CongratsMessage({
    Key? key,
    required this.game,
    required this.status,
    required this.faseNumber,
    required this.score,
    required this.medalsImage,
  }) : super(key: key);

  @override
  State<CongratsMessage> createState() => _CongratsMessageState();
}

class _CongratsMessageState extends State<CongratsMessage> {
  int currentMedal = 0;

  @override
  void initState() {
    infos();
    super.initState();
  }

  infos() async {
    final AppStore store = Modular.get();
    if (store.randomPoints >= store.medalPoints['gold']) {
      setState(() {
        currentMedal = 3;
      });
    } else if (store.randomPoints < store.medalPoints['gold'] &&
        store.randomPoints >= store.medalPoints['silver']) {
      setState(() {
        currentMedal = 2;
      });
    } else if (store.randomPoints < store.medalPoints['silver'] &&
        store.randomPoints >= store.medalPoints['copper']) {
      setState(() {
        currentMedal = 1;
      });
    }
  }

  @override
  void dispose() {
    final AppStore store = Modular.get();
    store.setOpenFase();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Color? cardColor = currentMedal != 0 ? Colors.lightBlue[400] : Colors.redAccent;
    return BackdropFilter(
      filter: ui.ImageFilter.blur(sigmaX: 3.0, sigmaY: 3.0),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey.shade200.withOpacity(0.5),
        ),
        child: Stack(
          children: [
            Align(
              alignment: Alignment.center,
              child: BackdropFilter(
                filter: ui.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.85,
                  width: MediaQuery.of(context).size.height * 0.85,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(
                        MediaQuery.of(context).size.height * 0.425),
                    color: Colors.grey.shade200.withOpacity(0.5),
                  ),
                ),
              ),
            ),
            Align(
              alignment: const Alignment(0, 0),
              child: SizedBox(
                height: MediaQuery.of(context).size.height * 0.60,
                width: MediaQuery.of(context).size.width * 0.55,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Image(
                        image: MemoryImage(
                              widget.medalsImage[currentMedal],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.15),
              child: Align(
                alignment: Alignment.topCenter,
                child: Container(
                  height: 60,
                  width: MediaQuery.of(context).size.width * 0.6,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80),
                      color: cardColor,
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 8,
                            color: Colors.lightBlue[300]!,
                            offset: const Offset(-1, 4),
                            spreadRadius: 1),
                        BoxShadow(
                          blurRadius: 6,
                          color: Colors.grey[800]!.withOpacity(0.6),
                          offset: const Offset(2, 2),
                          spreadRadius: -1,
                        ),
                      ]),
                  child: Center(
                    child: Text(
                      currentMedal != 0 ? 'Parabéns' : 'Tente Novamente',
                      style: const TextStyle(
                          color: Colors.white,
                          fontSize: 32,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              ),
            ),
            Align(
              alignment: const Alignment(0, 0.7),
              child: Container(
                width: 350,
                height: 55,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: GestureDetector(
                        onTap: () {
                          widget.game.overlays.remove('CongratsMessage');
                          widget.game.resumeEngine();
                          widget.game.joystick?.moveTo(
                            Vector2(
                              widget.game.player!.x,
                              widget.game.player!.y,
                            ),
                          );
                          print('resume');
                        },
                        child: Container(
                          height: 55,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55),
                              color: cardColor,
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 8,
                                    color: Colors.lightBlue[300]!,
                                    offset: const Offset(-1, 4),
                                    spreadRadius: 1),
                                BoxShadow(
                                  blurRadius: 6,
                                  color: Colors.grey[800]!.withOpacity(0.6),
                                  offset: const Offset(2, 2),
                                  spreadRadius: -1,
                                ),
                              ]),
                          child: const Center(
                            child: Text(
                              'Jogar Novamente',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Expanded(
                      flex: 2,
                      child: GestureDetector(
                        onTap: () {
                          widget.game.overlays.remove('CongratsMessage');
                          widget.game.resumeEngine();
                          widget.game.joystick?.moveTo(
                            Vector2(
                              widget.game.player!.x,
                              widget.game.player!.y,
                            ),
                          );
                          print('resume');
                        },
                        child: Container(
                          height: 55,
                          width: 150,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(55),
                              color: cardColor,
                              boxShadow: [
                                BoxShadow(
                                    blurRadius: 8,
                                    color: Colors.lightBlue[300]!,
                                    offset: const Offset(-1, 4),
                                    spreadRadius: 1),
                                BoxShadow(
                                  blurRadius: 6,
                                  color: Colors.grey[800]!.withOpacity(0.6),
                                  offset: const Offset(2, 2),
                                  spreadRadius: -1,
                                ),
                              ]),
                          child: Center(
                            child: Text(
                              currentMedal != 0 ? 'Concluir' : 'Jogar mais tarde',
                              style: const TextStyle(
                                  color: Colors.white,
                                  fontSize: 17,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
