import 'dart:ui';

import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

import '../dowload_flibook.dart';
import '../flipbook_view.dart';

class Loading extends StatefulWidget {
  final BonfireGame game;
  const Loading({
    Key? key,
    required this.game,
  }) : super(key: key);

  @override
  State<Loading> createState() => _LoadingState();
}

class _LoadingState extends State<Loading> {
  @override
  void initState() {
    super.initState();
    _initDir();
  }

  late String _dir;
  late Uri _dirtwo;

  _initDir() async {
    _dir = (await getApplicationDocumentsDirectory()).path;
    _dirtwo = (await getApplicationDocumentsDirectory()).uri;

    String download =
        await DownloadFlipBook(dir: _dir, dirtwo: _dirtwo).downloadZip();

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => FlipBookView(
          cam: download,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
      child: Container(
        height: double.infinity,
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.grey.shade200.withOpacity(0.5),
        ),
        child: Center(
          child: Container(
            height: 50,
            width: 250,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(80),
                color: Colors.lightBlue[400],
                boxShadow: [
                  BoxShadow(
                      blurRadius: 8,
                      color: Colors.lightBlue[300]!,
                      offset: const Offset(-1, 4),
                      spreadRadius: 1),
                  BoxShadow(
                    blurRadius: 6,
                    color: Colors.grey[800]!.withOpacity(0.6),
                    offset: const Offset(2, 2),
                    spreadRadius: -1,
                  ),
                ]),
            child: Center(
              child: Card(
                color: Colors.lightBlue[400],
                elevation: 0,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 5),
                      child: const CircularProgressIndicator(
                        color: Colors.white,
                      ),
                    ),
                    const Expanded(
                      child: Text(
                        'Fazendo Download...',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            fontWeight: FontWeight.bold),
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
