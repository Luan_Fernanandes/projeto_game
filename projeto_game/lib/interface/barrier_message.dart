import 'package:flutter/material.dart';

Widget barrierMessage() {
  return Card(
      child: Column(
    children: const [
      Text('Você precisa de mais pontos'),
      Text('Volte à fase anterior')
    ],
  ));
}

Widget openMessage(int number) {
  return Card(
      child: Column(
    children: [const Text('Parabéns'), Text('Vá para a fase $number')],
  ));
}
