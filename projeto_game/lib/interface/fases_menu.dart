import 'dart:typed_data';

import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:projeto_game/store/mobx_store.dart';

import '../decoration/getDirectory.dart';

class FasesMenu extends StatefulWidget {
  final BonfireGame game;

  const FasesMenu({
    Key? key,
    required this.game,
  }) : super(key: key);

  @override
  State<FasesMenu> createState() => _FasesMenuState();
}

class _FasesMenuState extends State<FasesMenu> {
  AppStore store = Modular.get();
  double alinhamento = 1.80;
  List<Uint8List> fasesImageGet = [];
  bool loadFaseImage = true;

  @override
  void initState() {
    super.initState();
    setLoad();
  }

  setLoad() async {
    fasesImageGet.clear();
    await store.setOpenFase();
    print(store.openFase);
    if (store.openFase.isNotEmpty) {
      for (var element in store.openFase) {
        await loadFaseOpen(element);
        if (fasesImageGet.length == store.openFase.length) {
          print('igual');
          print(fasesImageGet);
          setState(() {
            loadFaseImage = false;
          });
        } else {
          print("aqui1 ${fasesImageGet.length}");
          print("aqui2 ${store.openFase.length}");
        }
      }
    } else {
      loadFaseOpen('1');
      setState(() {
        loadFaseImage = false;
      });
    }
  }

  loadFaseOpen(String fasenumber) async {
    await GetDir(lc: 'assets/images/fases/Open $fasenumber.png')
        .localFileShowWidget()
        .then(
      (value) {
        setState(() {
          fasesImageGet.add(value);
        });
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedAlign(
      onEnd: setLoad,
      duration: const Duration(milliseconds: 100),
      alignment: Alignment(0, alinhamento),
      child: SizedBox(
        height: MediaQuery.of(context).size.height * 0.36,
        width: MediaQuery.of(context).size.width,
        child: Stack(
          children: [
            Positioned(
              bottom: 0,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.315,
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                    topLeft: Radius.circular(25),
                    topRight: Radius.circular(25),
                  ),
                  color: Colors.lightBlue[400],
                  boxShadow: [
                    BoxShadow(
                      blurRadius: 6,
                      color:
                          const Color.fromARGB(255, 7, 7, 7).withOpacity(0.6),
                      offset: const Offset(2, -4),
                      spreadRadius: -1,
                    ),
                  ],
                ),
                child: Container(
                  padding: const EdgeInsets.all(10),
                  child: loadFaseImage
                      ? const Center(
                          child: CircularProgressIndicator(
                            color: Colors.white,
                          ),
                        )
                      : ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: fasesImageGet.length,
                          itemBuilder: (ctx, i) {
                            return Container(
                              margin: const EdgeInsets.all(2),
                              height: 100,
                              width: 100,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: MemoryImage(fasesImageGet[i]),
                                    fit: BoxFit.cover),
                              ),
                            );
                          },
                        ),
                ),
              ),
            ),
            Align(
              alignment: Alignment.topCenter,
              child: GestureDetector(
                onTap: () {
                  if (alinhamento == 1) {
                    setState(() {
                      alinhamento = 1.80;
                    });
                  } else {
                    setState(() {
                      alinhamento = 1;
                    });
                  }
                  widget.game.joystick?.moveTo(
                    Vector2(
                      widget.game.player!.x,
                      widget.game.player!.y,
                    ),
                  );
                },
                child: Container(
                  height: 35,
                  width: 100,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80),
                      color: Colors.lightBlue[400],
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 8,
                            color: Colors.lightBlue[300]!,
                            offset: const Offset(-1, 4),
                            spreadRadius: 1),
                        BoxShadow(
                          blurRadius: 6,
                          color: Colors.grey[800]!.withOpacity(0.6),
                          offset: const Offset(2, 2),
                          spreadRadius: -1,
                        ),
                      ]),
                  child: Center(
                    child: Card(
                      color: Colors.lightBlue[400],
                      elevation: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: const [
                          Text(
                            'Fases',
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            Icons.lock_open,
                            size: 20,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
