import 'package:bonfiremodify/bonfire.dart';
import 'package:flutter/material.dart';
import 'package:fluttermoji/fluttermoji.dart';

Widget backButton(BonfireGame game, Function onClick) {
  return Align(
      alignment: const Alignment(-0.96, -0.85),
      child: GestureDetector(
        child: Image.asset(
          'assets/images/utils/back_icon.png',
          height: 50,
        ),
        onTap: () => onClick,
      ));
}

Widget longButtonWithPng(
  BonfireGame game,
  Function onClick,
  String label,
) {
  return Align(
    alignment: const Alignment(0.3, -0.98),
    child: GestureDetector(
        child: Stack(
      children: [
        Image.asset(
          'assets/images/utils/long_button.png',
          height: 80,
        ),
        Positioned(
          left: 45,
          top: 15,
          child: Card(
            color: Colors.lightBlue[400],
            elevation: 0,
            child: Text(
              label,
              style: const TextStyle(
                  color: Colors.white,
                  fontSize: 24,
                  fontWeight: FontWeight.bold),
            ),
          ),
        )
      ],
    )),
  );
}

Widget longButtonWithoutPng(
    BonfireGame game, Function onClick, String label, Alignment alignment) {
  return Align(
    alignment: alignment,
    child: GestureDetector(
      child: Material(
        borderRadius: BorderRadius.circular(80),
        elevation: 4,
        child: Container(
          height: 35,
          width: 120,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              color: Colors.lightBlue[400],
              boxShadow: [
                BoxShadow(
                    blurRadius: 8,
                    color: Colors.lightBlue[300]!,
                    offset: const Offset(-1, 4),
                    spreadRadius: 1),
                BoxShadow(
                  blurRadius: 6,
                  color: Colors.grey[800]!.withOpacity(0.6),
                  offset: const Offset(2, 2),
                  spreadRadius: -1,
                ),
              ]),
          child: Center(
            child: Card(
              color: Colors.lightBlue[400],
              elevation: 0,
              child: Text(
                label,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}

Widget circleAvatarButtonWithGear(BonfireGame game, Function onClick) {
  return Align(
    alignment: const Alignment(0.95, -0.84),
    child: Stack(children: [
      Container(
        height: 50,
        decoration: const BoxDecoration(
            shape: BoxShape.circle, color: Color.fromARGB(255, 160, 219, 247)),
        child: FluttermojiCircleAvatar(
          backgroundColor: Colors.lightBlue[400],
        ),
      ),
      const Positioned(
        bottom: 0,
        left: 45,
        child: Icon(
          Icons.settings,
          size: 20,
          color: Color.fromARGB(213, 7, 56, 88),
        ),
      )
    ]),
  );
}

Widget playButton(BonfireGame game, Function onClick, String label,
    Alignment alignment, context) {
  return Align(
    alignment: alignment,
    child: GestureDetector(
      onTap: () {
        onClick();
      },
      child: Material(
        borderRadius: BorderRadius.circular(80),
        elevation: 4,
        child: Container(
          height: 60,
          width: 140,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(80),
              color: Colors.lightBlue[400],
              boxShadow: [
                BoxShadow(
                    blurRadius: 8,
                    color: Colors.lightBlue[300]!,
                    offset: const Offset(-1, 4),
                    spreadRadius: 1),
                BoxShadow(
                  blurRadius: 6,
                  color: Colors.grey[800]!.withOpacity(0.6),
                  offset: const Offset(2, 2),
                  spreadRadius: -1,
                ),
              ]),
          child: Center(
            child: Card(
              color: Colors.lightBlue[400],
              elevation: 0,
              child: Text(
                label,
                style: const TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
      ),
    ),
  );
}
