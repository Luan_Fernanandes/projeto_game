import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'download_game_files.dart';
import 'map.dart';

import 'dart:ui' as ui;

class DownloadGameView extends StatefulWidget {
  final ui.Image characterimage;
  final bool map;
  const DownloadGameView({
    Key? key,
    required this.characterimage,
    required this.map,
  }) : super(key: key);

  @override
  State<DownloadGameView> createState() => _DownloadGameViewState();
}

class _DownloadGameViewState extends State<DownloadGameView> {
  @override
  void initState() {
    super.initState();
    _initDir();
  }

  late String _dir;
  late Uri _dirtwo;
  late String mapName;

  _initDir() async {
    _dir = (await getApplicationDocumentsDirectory()).path;
    _dirtwo = (await getApplicationDocumentsDirectory()).uri;

    if (widget.map) {
      setState(() {
        mapName = 'assets_game_mapa_2.zip';
      });
    }else{
      setState(() {
        mapName = 'assets_game_mapa_1.zip';
      });
    }

    String download =
        await DownloadGameFiles(dir: _dir, dirtwo: _dirtwo, mapZipName: mapName).downloadZip();
      
    String passFolderName = mapName.replaceAll('.zip', '');
    print(passFolderName);

    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => Map(
          characterimage: widget.characterimage,
          cam: download,
          folderName: passFolderName,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Stack(
        children: [
          Container(
            width: double.infinity,
            height: double.infinity,
            decoration: const BoxDecoration(color: Colors.white),
          ),
          BackdropFilter(
            filter: ui.ImageFilter.blur(sigmaX: 10.0, sigmaY: 10.0),
            child: Container(
              height: double.infinity,
              width: double.infinity,
              decoration: BoxDecoration(
                color: Colors.grey.shade200.withOpacity(0.5),
              ),
              child: Center(
                child: Container(
                  height: 50,
                  width: 250,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(80),
                      color: Colors.lightBlue[400],
                      boxShadow: [
                        BoxShadow(
                            blurRadius: 8,
                            color: Colors.lightBlue[300]!,
                            offset: const Offset(-1, 4),
                            spreadRadius: 1),
                        BoxShadow(
                          blurRadius: 6,
                          color: Colors.grey[800]!.withOpacity(0.6),
                          offset: const Offset(2, 2),
                          spreadRadius: -1,
                        ),
                      ]),
                  child: Center(
                    child: Card(
                      color: Colors.lightBlue[400],
                      elevation: 0,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Container(
                            margin: const EdgeInsets.symmetric(horizontal: 5),
                            child: const CircularProgressIndicator(
                              color: Colors.white,
                            ),
                          ),
                          const Expanded(
                            child: Text(
                              'Fazendo Download...',
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                              overflow: TextOverflow.ellipsis,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
