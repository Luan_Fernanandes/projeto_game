import 'package:projeto_game/database/app_database.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseAdaptor {
  final AppDatabase database = AppDatabase();

  Future<void> saveData({
    required String tableName,
    required Map<String, dynamic> data,
  }) async {
    final Database db = await database.initdb();

    if (tableName == 'fasepoints') {
      final existingRelation = await db.query(
        'fasepoints',
        where: 'id = ?',
        whereArgs: [data["id"]],
      );

      if (existingRelation.isEmpty) {
        await db.insert(tableName, data);
      } else {
        await db.update(
          tableName,
          data,
          where: "id = ?",
          whereArgs: [existingRelation.first['id']],
        );
      }

      return;
    }

    final alreadyExists = data["id"] != null
        ? await db.query(
            tableName,
            where: "id = ?",
            whereArgs: [data["id"] as int],
          )
        : [];

    if (alreadyExists.isNotEmpty) {
      await updateData(id: data["id"] as int, data: data, tableName: tableName);
    } else {
      await db.insert(tableName, data);
    }
  }

  Future<void> updateData({
    required int id,
    required Map<String, dynamic> data,
    required String tableName,
  }) async {
    final Database db = await database.initdb();

    await db.update(tableName, data, where: 'id = ?', whereArgs: [id]);
  }

  Future<List<Map<String, dynamic>>> readData(
      {required String tableName, int? id}) async {
    final Database db = await database.initdb();
    if (id != null) {
      final List<Map<String, dynamic>> result =
          await db.query(tableName, where: 'id = ?', whereArgs: [id]);
      return result;
    } else {
      final result = await db.query(tableName);
      return result;
    }
  }
}
