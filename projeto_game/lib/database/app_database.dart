import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class AppDatabase {
  Future<Database> initdb() async {
    final String path = join(await getDatabasesPath(), 'app.db');
    return openDatabase(
      path,
      version: 1,
      onDowngrade: onDatabaseDowngradeDelete,
      onCreate: (Database db, int newerVersion) async {
        await _createFasePointsTable(db);
        await _createSecundaryFaseTable(db);
        await _createFasePointsSecundaryTable(db);
      },
    );
  }

  Future<void> _createFasePointsTable(Database db) async {
    await db.execute(
      '''CREATE TABLE fasepoints(
      id INTEGER PRIMARY KEY,
      fase TEXT,
      point INTEGER,
      open INTEGER);''',
    );
  }

  Future<void> _createSecundaryFaseTable(Database db) async {
    await db.execute(
      '''CREATE TABLE secundaryfase(
      id INTEGER PRIMARY KEY,
      fase TEXT,
      point INTEGER,
      open INTEGER);''',
    );
  }

  Future<void> _createFasePointsSecundaryTable(Database db) async {
    await db.execute(
      '''CREATE TABLE fasesecundarypoints(
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      primary_id INTEGER,
      secundary_id INTEGER,
      FOREIGN KEY (primary_id) REFERENCES fasepoints(id) ON DELETE CASCADE
      FOREIGN KEY (secundary_id) REFERENCES secundaryfase(id) ON DELETE CASCADE);''',
    );
  }
}
