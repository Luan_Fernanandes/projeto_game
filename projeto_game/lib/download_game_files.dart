import 'dart:io';

import 'package:archive/archive.dart';
import 'package:dio/dio.dart';

class DownloadGameFiles {
  final String dir;
  final Uri dirtwo;
  final String mapZipName;
  DownloadGameFiles({
    required this.dir,
    required this.dirtwo,
    required this.mapZipName,
  });

  late bool _downloading = false;
  Future<File> _downloadFile(String url, String fileName) async {
    final req = await Dio().get(
      url,
      options: Options(
        responseType: ResponseType.bytes,
        followRedirects: false,
        receiveTimeout: 0,
      ),
    );
    var file = File('$dir/$fileName');
    return file.writeAsBytes(req.data);
  }

  Future<String> downloadZip() async {
    //print(dir);
    // print(dirtwo);
    _downloading = true;

    var zippedFile = await _downloadFile('https://luanfern.mxw.com.br/download/$mapZipName', mapZipName);
    await unarchiveAndSave(zippedFile);
    _downloading = false;

    return dir.toString();
  }

  unarchiveAndSave(var zippedFile) async {
    var bytes = zippedFile.readAsBytesSync();
    var archive = ZipDecoder().decodeBytes(bytes);
    for (var file in archive) {
      var fileName = '$dir/${file.name}';
      if (file.isFile) {
        var outFile = File(fileName);
        //print('File:: ' + outFile.path);
        outFile = await outFile.create(recursive: true);
        await outFile.writeAsBytes(file.content);
      }
    }
  }
}
