import 'package:flame_audiomodify/flame_audio.dart';

class SoundsManager {
  static Future initialize() async {
    FlameAudio.bgm.initialize();

    await FlameAudio.audioCache.loadAll([
      '/assets/audio/sfx/button_click.ogg',
    ]);
  }

  static void fadeInBgm() {
    double volume = 0.05;
    final stopwatch = Stopwatch()..start();
    (timer) {
      if (stopwatch.elapsed.inSeconds == 4) {
        timer.cancel();
        stopwatch.stop();
      }
      volume += 0.05;
      FlameAudio.bgm.audioPlayer?.setVolume(volume);
    };
  }

  static void buttonClick() {
    FlameAudio.audioCache.play('/assets/audio/sfx/button_click.ogg', volume: 0.6);
  }

  static Future<void> playChopinRevelation() async {
    try {
      await FlameAudio.bgm.play('/assets/audio/bgm/Seaadventure.mp3', volume: 0.3);
    } catch (e) {
      print('ERRO:: $e');
    }
    //fadeInBgm();
  }
}
