import 'dart:typed_data';

import 'package:flutter/material.dart';

class testImage extends StatefulWidget {
  final Uint8List characterimage;
  const testImage({ Key? key, required this.characterimage, }) : super(key: key);

  @override
  State<testImage> createState() => _testImageState();
}

class _testImageState extends State<testImage> {
  @override
  Widget build(BuildContext context) {
    return MediaQuery(
      data: const MediaQueryData(),
      child:  MaterialApp(home: Center(
        child: Image.memory(widget.characterimage),
      ),),
);
  }
}