import 'package:flutter_modular/flutter_modular.dart';
import 'package:projeto_game/avatar.dart';
import 'package:projeto_game/store/mobx_store.dart';

class AppModular extends Module {
  @override
  List<ModularRoute> get routes =>
      [ChildRoute('/', child: (context, args) => const NewPage())];

  @override
  List<Bind<Object>> get binds => [
        Bind<AppStore>((i) => AppStore()),
      ];
}
